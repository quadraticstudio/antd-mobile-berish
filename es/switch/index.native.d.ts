/// <reference types="react" />
import SwitchProps from './PropsType';
declare const AntmSwitch: (props: SwitchProps) => JSX.Element;
export default AntmSwitch;
