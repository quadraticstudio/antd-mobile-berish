import _classCallCheck from 'babel-runtime/helpers/classCallCheck';
import _createClass from 'babel-runtime/helpers/createClass';
import _possibleConstructorReturn from 'babel-runtime/helpers/possibleConstructorReturn';
import _inherits from 'babel-runtime/helpers/inherits';
/* tslint:disable:jsx-no-multiline-js */
import React from 'react';
import ReactDOM from 'react-dom';
import classnames from 'classnames';
import CustomKeyboard from './CustomKeyboard';
import Portal from './Portal';
import { addClass, removeClass } from '../_util/class';
var IS_REACT_16 = !!ReactDOM.createPortal;
var customNumberKeyboard = null;
var hasPortal = false;

var NumberInput = function (_React$Component) {
    _inherits(NumberInput, _React$Component);

    function NumberInput(props) {
        _classCallCheck(this, NumberInput);

        var _this = _possibleConstructorReturn(this, (NumberInput.__proto__ || Object.getPrototypeOf(NumberInput)).call(this, props));

        _this.onChange = function (value) {
            if (!('value' in _this.props)) {
                _this.setState({ value: value.target.value });
            }
            _this.props.onChange(value);
        };
        _this.addBlurListener = function () {
            document.addEventListener('click', _this.doBlur, false);
        };
        _this.removeBlurListener = function () {
            document.removeEventListener('click', _this.doBlur, false);
        };
        _this.saveRef = function (el) {
            if (IS_REACT_16) {
                customNumberKeyboard = el;
            }
        };
        _this.doBlur = function (ev) {
            var value = _this.state.value;

            if (ev.target !== _this.inputRef) {
                _this.onInputBlur(value);
            }
        };
        _this.unLinkInput = function () {
            if (customNumberKeyboard && customNumberKeyboard.linkedInput && customNumberKeyboard.linkedInput === _this) {
                customNumberKeyboard.linkedInput = null;
                addClass(customNumberKeyboard.antmKeyboard, _this.props.keyboardPrefixCls + '-wrapper-hide');
            }
            // for unmount
            _this.removeBlurListener();
        };
        _this.onInputBlur = function (value) {
            var focus = _this.state.focus;

            if (focus) {
                _this.setState({
                    focus: false
                });
                _this.props.onBlur(value);
                setTimeout(function () {
                    _this.unLinkInput();
                }, 50);
            }
        };
        _this.onInputFocus = function () {
            var value = _this.state.value;

            _this.props.onFocus(value);
            _this.setState({
                focus: true
            }, function () {
                customNumberKeyboard.linkedInput = _this;
                removeClass(customNumberKeyboard.antmKeyboard, _this.props.keyboardPrefixCls + '-wrapper-hide');
                customNumberKeyboard.confirmDisabled = value === '';
                if (value === '') {
                    addClass(customNumberKeyboard.confirmKeyboardItem, _this.props.keyboardPrefixCls + '-item-disabled');
                } else {
                    removeClass(customNumberKeyboard.confirmKeyboardItem, _this.props.keyboardPrefixCls + '-item-disabled');
                }
            });
        };
        _this.onKeyboardClick = function (KeyboardItemValue) {
            var maxLength = _this.props.maxLength;
            var value = _this.state.value;
            var onChange = _this.onChange;

            var valueAfterChange = void 0;
            // 删除键
            if (KeyboardItemValue === 'delete') {
                valueAfterChange = value.substring(0, value.length - 1);
                onChange({ target: { value: valueAfterChange } });
                // 确认键
            } else if (KeyboardItemValue === 'confirm') {
                valueAfterChange = value;
                onChange({ target: { value: valueAfterChange } });
                _this.onInputBlur(value);
                // 收起键
            } else if (KeyboardItemValue === 'hide') {
                valueAfterChange = value;
                _this.onInputBlur(valueAfterChange);
            } else {
                if (maxLength !== undefined && +maxLength >= 0 && (value + KeyboardItemValue).length > maxLength) {
                    valueAfterChange = (value + KeyboardItemValue).substr(0, maxLength);
                    onChange({ target: { value: valueAfterChange } });
                } else {
                    valueAfterChange = value + KeyboardItemValue;
                    onChange({ target: { value: valueAfterChange } });
                }
            }
            customNumberKeyboard.confirmDisabled = valueAfterChange === '';
            if (valueAfterChange === '') {
                addClass(customNumberKeyboard.confirmKeyboardItem, _this.props.keyboardPrefixCls + '-item-disabled');
            } else {
                removeClass(customNumberKeyboard.confirmKeyboardItem, _this.props.keyboardPrefixCls + '-item-disabled');
            }
        };
        _this.onFakeInputClick = function () {
            _this.focus();
        };
        _this.focus = function () {
            // this focus may invocked by users page button click, so this click may trigger blurEventListener at the same time
            _this.removeBlurListener();
            var focus = _this.state.focus;

            if (!focus) {
                _this.onInputFocus();
            }
            setTimeout(function () {
                _this.addBlurListener();
            }, 50);
        };
        _this.state = {
            focus: false,
            value: props.value || ''
        };
        return _this;
    }

    _createClass(NumberInput, [{
        key: 'componentWillReceiveProps',
        value: function componentWillReceiveProps(nextProps) {
            if ('value' in nextProps) {
                this.setState({
                    value: nextProps.value
                });
            }
        }
    }, {
        key: 'componentDidMount',
        value: function componentDidMount() {
            if (!IS_REACT_16 && !customNumberKeyboard) {
                this.renderCustomKeyboard();
            }
        }
    }, {
        key: 'componentWillUnmount',
        value: function componentWillUnmount() {
            // focus:true unmount 不能触发 blur
            if (this.state.focus) {
                this.props.onBlur(this.state.value);
            }
            this.unLinkInput();
        }
    }, {
        key: 'getComponent',
        value: function getComponent() {
            var _props = this.props,
                keyboardPrefixCls = _props.keyboardPrefixCls,
                confirmLabel = _props.confirmLabel;

            if (!hasPortal || !IS_REACT_16) {
                return React.createElement(CustomKeyboard, { ref: this.saveRef, onClick: this.onKeyboardClick, preixCls: keyboardPrefixCls, confirmLabel: confirmLabel });
            }
            return null;
        }
    }, {
        key: 'getContainer',
        value: function getContainer() {
            var container = document.querySelector('#' + this.props.keyboardPrefixCls + '-container');
            if (!container) {
                container = document.createElement('div');
                container.setAttribute('id', this.props.keyboardPrefixCls + '-container');
                document.body.appendChild(container);
            }
            this.container = container;
            return container;
        }
    }, {
        key: 'renderCustomKeyboard',
        value: function renderCustomKeyboard() {
            customNumberKeyboard = ReactDOM.unstable_renderSubtreeIntoContainer(this, this.getComponent(), this.getContainer());
        }
    }, {
        key: 'renderPortal',
        value: function renderPortal() {
            var _this2 = this;

            if (!IS_REACT_16) {
                return null;
            }
            var portal = React.createElement(
                Portal,
                { getContainer: function getContainer() {
                        return _this2.getContainer();
                    } },
                this.getComponent()
            );
            // make sure one keyboard is been created in react@16
            // https://github.com/ant-design/ant-design-mobile/issues/2065
            hasPortal = true;
            return portal;
        }
    }, {
        key: 'render',
        value: function render() {
            var _this3 = this;

            var _props2 = this.props,
                placeholder = _props2.placeholder,
                disabled = _props2.disabled,
                editable = _props2.editable,
                moneyKeyboardAlign = _props2.moneyKeyboardAlign;
            var _state = this.state,
                focus = _state.focus,
                value = _state.value;

            var preventKeyboard = disabled || !editable;
            var fakeInputCls = classnames('fake-input', {
                focus: focus,
                'fake-input-disabled': disabled
            });
            var fakeInputContainerCls = classnames('fake-input-container', {
                'fake-input-container-left': moneyKeyboardAlign === 'left'
            });
            return React.createElement(
                'div',
                { className: fakeInputContainerCls },
                value === '' && React.createElement(
                    'div',
                    { className: 'fake-input-placeholder' },
                    placeholder
                ),
                React.createElement(
                    'div',
                    { className: fakeInputCls, ref: function ref(el) {
                            return _this3.inputRef = el;
                        }, onClick: preventKeyboard ? function () {} : this.onFakeInputClick },
                    value
                ),
                this.renderPortal()
            );
        }
    }]);

    return NumberInput;
}(React.Component);

NumberInput.defaultProps = {
    onChange: function onChange() {},
    onFocus: function onFocus() {},
    onBlur: function onBlur() {},
    placeholder: '',
    disabled: false,
    editable: true,
    prefixCls: 'am-input',
    keyboardPrefixCls: 'am-number-keyboard'
};
export default NumberInput;