/// <reference types="react" />
import * as React from 'react';
import { GridProps as BasePropsType } from './PropsType';
export interface GridProps extends BasePropsType {
    prefixCls?: string;
    className?: string;
    square?: boolean;
    activeClassName?: string;
    activeStyle?: object;
}
export default class Grid extends React.Component<GridProps, any> {
    static defaultProps: {
        data: never[];
        hasLine: boolean;
        isCarousel: boolean;
        columnNum: number;
        carouselMaxRow: number;
        prefixCls: string;
        square: boolean;
        itemStyle: {};
    };
    state: {
        initialSlideWidth: number;
    };
    componentDidMount(): void;
    renderCarousel: (rowsArr: any, pageCount: any, rowCount: any) => any[];
    renderItem: (dataItem: any, index: number, columnNum: number, renderItem: any) => JSX.Element;
    getRows: (rowCount: any, dataLength: any) => any[];
    render(): JSX.Element | null;
}
