/// <reference types="react" />
import * as React from 'react';
export interface TabBarProps {
    barTintColor?: string;
    tintColor?: string;
    unselectedTintColor?: string;
    children?: any;
    prefixCls?: string;
    className?: string;
    hidden?: boolean;
    placeholder?: React.ReactNode;
    /** default: false */
    animated?: boolean;
    /** default: false */
    swipeable?: boolean;
    /** rn android only**/
    styles?: any;
    noRenderContent?: boolean;
}
export interface TabBarItemProps {
    badge?: string | number;
    onPress?: () => void;
    selected?: boolean;
    icon?: any;
    selectedIcon?: any;
    title: string;
    dot?: boolean;
    prefixCls?: string;
    style?: any;
}
