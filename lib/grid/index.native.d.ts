/// <reference types="react" />
import * as React from 'react';
import { GridProps as BasePropsType } from './PropsType';
export interface GridProps extends BasePropsType {
    styles?: any;
}
export default class Grid extends React.Component<GridProps, any> {
    static defaultProps: {
        data: never[];
        hasLine: boolean;
        isCarousel: boolean;
        columnNum: number;
        carouselMaxRow: number;
        styles: any;
        itemStyle: {};
    };
    getFlexItemStyle(): {
        height: number;
        borderRightWidth: number;
    };
    render(): JSX.Element;
}
