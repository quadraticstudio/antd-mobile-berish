declare const _default: {
    showActionSheetWithOptions(config: any, callback: any): void;
    showShareActionSheetWithOptions(config: any, failureCallback?: Function | undefined, successCallback?: Function | undefined): void;
    close(): void;
};
export default _default;
