/// <reference types="react" />
import * as React from 'react';
import { IStepsStyle } from './style/index.native';
export interface StepsProps {
    direction?: 'vertical' | 'horizontal';
    current?: number;
    width?: number;
    size?: string;
    finishIcon?: string;
    styles?: any;
}
export interface IStepsNativeProps extends StepsProps {
    styles?: IStepsStyle;
}
export default class Steps extends React.Component<IStepsNativeProps, any> {
    static Step: any;
    static defaultProps: {
        direction: string;
        styles: any;
    };
    constructor(props: any);
    onLayout: (e: any) => void;
    render(): JSX.Element;
}
